# Cemantix Player - A useless python script

Python Script that play and find today's word for you on the popular game Cemantix. 

https://cemantix.herokuapp.com/

It's been quickly put together with great help of this pretrained model thingy :
https://fauconnier.github.io/#data

## Requirements 

### Software

+ anaconda (https://www.anaconda.com/products/individual)
+ spyder5 (installed in anaconda)
+ Python 3.9.7 (installed in anaconda)

### Python librairies

+ pandas 1.3.4
+ requests 2.26.0
+ json 2.0.9 (probably already installed ?)
+ numpy 1.20.3
+ gensim 4.1.2

### Pre Trained thingy

https://embeddings.net/embeddings/frWiki_no_phrase_no_postag_700_cbow_cut100.bin

Found on thing page : https://fauconnier.github.io/#data

This thing must be put in the same folder as everything else.

## Files

+ `breaklib.py` : Some functions
+ `smartbreaker` : Some more functions involving the pretrained model thingy
+ `load_word.py` : Remove the words from the word list that Cemantix doesn't know
+ `words.txt` : A list of word to bootstrap the word search (I've stole this from another github/gitlab, I don't remember where, I'm sorry.)
+ `wordlist.txt` : A better wordlist, filtered, extracted from http://www.encyclopedie-incomplete.com/?Les-600-Mots-Francais-Les-Plus
+ `smarter.py` : The file to run
+ `README.md` : This file.

## Setting everything up

You may want to install Anaconda with spyder5 and python before.

1. Downloads those files : `git clone git@gitlab.com:Karnaesca/cemantix-player.git`
2. Go in the new folder : `cd cemantix-player`
3. Get the pretrained model thingy :`wget https://embeddings.net/embeddings/frWiki_no_phrase_no_postag_700_cbow_cut100.bin`
4. Install python librairies in your environnement (anaconda ?) `pip install -r requirements.txt`
5. Run `smarter.py`

## Usage

Run the `smarter.py` file.

And you should see something like this :

![](img/SPOILER_SPOIL_cemantix.png)
