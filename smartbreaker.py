from gensim.models import KeyedVectors
import numpy as np

fname = "frWiki_no_phrase_no_postag_700_cbow_cut100.bin"

model = KeyedVectors.load_word2vec_format(fname, binary=True, unicode_errors="ignore")

def get_similar(word,n=10):
    try:
        return np.asarray(model.most_similar(word,topn=n))[:,0]
    except:
        return np.array([])