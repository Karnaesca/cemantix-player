import warnings
warnings.filterwarnings("ignore")
import requests
import pandas as pd
import json
import time
import numpy as np
import smartbreaker as sb
import breaklib as bl

def get_iloc_first_false(smarted):
    t = smarted.values
    for i,e in enumerate(t):
        if e==False:
            return i
    return None

print("Start playing Cemantix !\n")
#init
cols = ["num","percentile","score","solvers","word","smarted"]
pcols = ["percentile","score","word"]
scores = pd.DataFrame(columns=cols)

newword = bl.getrandomwords(1)

print(f"First random words :\n")

df1 = bl.getscore(newword)
print(f"{df1.word.values[0]} - {df1.score.values[0]}")
scores = bl.update(scores,df1)
   
while scores.head(1).score.values[0] < 0.05 or len(scores)<20:
    newword = bl.getrandomwords(1)
    df1 = bl.getscore(newword)
    print(f"{df1.word.values[0]} - {df1.score.values[0]}")
    scores = bl.update(scores,df1)

    
print("\nRun the big brain now:\n")

#print("begin loop")
while scores.smarted.sum() < len(scores):
    index = scores[scores.smarted==False].head(1).index.values[0]
    scores["smarted"][index] = True
    tmpscore = scores.head(1).score
    #print(f"Using word : {scores.head(1).word.values}")
    for guess in sb.get_similar(scores.word[index],50):
        #print(f"Guess : {guess}")
        if guess in scores.word.values:
            #print(f"Already tested : {guess}")
            pass
        else:
            df1 = bl.getscore(guess)
            scores = bl.update(scores,df1)
            #print(scores.head(10))
            if (scores.head(1).score.item() > tmpscore.item()):
                tmpscore = scores.head(1).score
                print(f"Best guess : {scores.head(1).word.values[0]} - {scores.head(1).score.values[0]}")
                break
            if scores.head(1).score.item() == 1:
                break
    if scores.head(1).score.item() == 1:
        break
if scores.head(1).score.item() == 1:
    print()
    print(f"SUCCESS : {scores.head(1).word.values[0]}")
    print(f"Word Tested : {len(scores)}")
    print()
    print(scores[pcols].head(10))
else:
    print()
    print("No fucking clue, sorry mate")
