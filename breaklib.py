import requests
import pandas as pd
import json
import time
import numpy as np

INDEX = pd.Index([0])

def getrandomwords(n=10):
    fname = "wordlist.txt"
    with open(fname,"r") as f:
        s = f.read()  
    wordlist = np.asarray(s.split())
    np.random.shuffle(wordlist)
    return wordlist[0:n]
    

def score(word):
    url = "https://cemantix.herokuapp.com/score"
    obj = {"word":str(word)}
    
    r = requests.post(url,data=obj)
     
    while r.status_code != 200 :
        print(f"Fail to connect {r.status_code}")
        r = requests.post(url,data=obj)
        
    if r.status_code == 200:
        data = json.loads(r.text)
        #print(data)
        df = pd.json_normalize(data)
        return df

def getscore(word,delay=0):
    df1 = score(word)
    df1["word"]=word
    df1["smarted"]=False
    
    error_spam = ("error" in df1.keys()) and (df1.error.values[0] == 'Vous tapez trop vite pour moi.<br />Veuillez ré-essayer.')
    while error_spam :
        #print(f"[warning] : spam")
        time.sleep(delay)
        df1 = score(word)
        df1["word"]=word
        df1["smarted"]=False
        error_spam = ("error" in df1.keys()) and (df1.error.values[0] == 'Vous tapez trop vite pour moi.<br />Veuillez ré-essayer.')
    global INDEX
    #print(f"INDEX {INDEX}")
    df1.index = INDEX
    INDEX += 1
    #print(f"INDEX {INDEX}")
    return df1     

def update(scores,df):
    scores = pd.concat([scores,df])
    scores.dropna(inplace=True,subset=["score"])
    #scores.drop_duplicates("word",inplace=True,ignore_index=True)
    scores.drop_duplicates("word",inplace=True)
    scores.sort_values(by=["score"],ascending=False,inplace=True)        
    return scores           

    
